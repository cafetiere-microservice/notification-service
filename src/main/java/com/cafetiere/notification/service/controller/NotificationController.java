package com.cafetiere.notification.service.controller;

import com.cafetiere.notification.service.model.NotificationRequest;
import com.cafetiere.notification.service.FirebaseService.NotificationService;
import com.google.firebase.messaging.FirebaseMessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/notification")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @PostMapping(path = "/", produces = {"application/json"})
    public ResponseEntity<String> sendNotification(@RequestBody NotificationRequest notificationRequest) throws FirebaseMessagingException {
        return ResponseEntity.ok(notificationService.sendNotification(notificationRequest));
    }

    @PostMapping(path = "/{token}")
    public ResponseEntity<String> subscribe(@PathVariable(value = "token")String token) throws FirebaseMessagingException {
        return ResponseEntity.ok(notificationService.subscribeToTopic(token));
    }
}
