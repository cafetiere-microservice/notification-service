package com.cafetiere.notification.service.FirebaseService;

import com.cafetiere.notification.service.model.NotificationRequest;
import com.google.firebase.messaging.*;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class NotificationServiceImpl implements NotificationService{
    @Override
    public String sendNotification(NotificationRequest notificationRequest) throws FirebaseMessagingException {
        Notification notification = Notification.builder()
                .setTitle(notificationRequest.getTitle())
                .setBody(notificationRequest.getBody())
                .build();

        Message message = Message.builder()
                .setNotification(notification)
                .setTopic("cafetiere")
                .putData("content",  notificationRequest.getTitle())
                .putData("body", notificationRequest.getBody())
                .build();

        FirebaseMessaging.getInstance().send(message);
        return "success sending notification";
    }

    @Override
    public String subscribeToTopic(String token) throws FirebaseMessagingException {
        FirebaseMessaging.getInstance().subscribeToTopic(Collections.singletonList(token), "cafetiere");
        return "success subscribing";
    }
}
