package com.cafetiere.notification.service.FirebaseService;

import com.cafetiere.notification.service.model.NotificationRequest;
import com.google.firebase.messaging.FirebaseMessagingException;

public interface NotificationService {
    String sendNotification(NotificationRequest notificationRequest) throws FirebaseMessagingException;
    String subscribeToTopic(String token) throws FirebaseMessagingException;
}
