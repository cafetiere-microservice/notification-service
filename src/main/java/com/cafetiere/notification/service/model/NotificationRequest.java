package com.cafetiere.notification.service.model;

public class NotificationRequest {
    private String title;
    private String body;

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public NotificationRequest(String title, String body) {
        this.title = title;
        this.body = body;
    }
}
