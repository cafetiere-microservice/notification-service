package com.cafetiere.notification.service.controller;

import com.cafetiere.notification.service.model.NotificationRequest;
import com.cafetiere.notification.service.FirebaseService.NotificationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = NotificationController.class)
public class NotificationControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private NotificationService notificationService;

    private NotificationRequest request;

    @BeforeEach
    public void setUp() {
        request = new NotificationRequest("title", "body");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testSendNotification() throws Exception {
        when(notificationService.sendNotification(request)).thenReturn("success sending notification");
        mvc.perform(post("/notification/")
                .contentType(MediaType.APPLICATION_JSON).content(mapToJson(request)))
                .andExpect(status().isOk());
    }

    @Test
    public void testSubscribe() throws Exception {
        when(notificationService.subscribeToTopic("randomtoken")).thenReturn("success subscribing");
        mvc.perform(post("/notification/randomtoken"))
                .andExpect(status().isOk());
    }
}
